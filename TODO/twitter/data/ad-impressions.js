window.YTD.ad_impressions.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 04:18:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247270257133268992",
            "tweetText" : "Revolutionieren Sie Ihren Unterricht mit Microsoft Surface und Workplace as a Service von ALSO. Wie sparen andere Bildungseinrichtungen Geld, Zeit und Stress mit WaaS? Finden Sie es hier heraus.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Business-IT",
            "screenName" : "@bITexperts_DE"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UZH_ch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel_en"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniLuzern"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@phd_UniBasel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@inf_unibe"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IntOfficeUnibas"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HydroUniBern"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kriPoUZH"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sub_unibe"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 04:22:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245267569755463681",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
            "urls" : [ "https://t.co/I4grLgGEqj" ],
            "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:58:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "SGKB News",
            "screenName" : "@sgkb_news"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ReutersBiz"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FortuneMagazine"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@businessinsider"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TEDTalks"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MariaBartiromo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MarketWatch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SRF"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:11:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:26:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1248068111892447243",
            "tweetText" : "The 120Hz refresh rate was a key breakthrough in #OPPOFindX2Series' Ultra Vision Screen, but also a piece in a bigger puzzle. 🔬\n\nClick below and dive into the full story. #FindMore 📱",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "OPPO",
            "screenName" : "@oppo"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ForbesTech"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AndroidAuth"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MKBHD"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AndroidDev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@androidcentral"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:35:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Orchid",
            "screenName" : "@OrchidProtocol"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EFF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AndroidDev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          } ],
          "impressionTime" : "2020-04-09 03:11:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247810756118224896",
            "tweetText" : "#Webinar am 15. April 2020, 13:00 - 13:30 Uhr: Vom Prozess zum Workflow! Automatisieren von Prozessen in der Nintex Workflow Cloud. \n\nJetzt anmelden!\n#ProcessExcellence #BusinessProcess #ProcessAutomation",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "NLA",
            "screenName" : "@NLA_Processes"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:17:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514947989671937",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 03:17:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 07:45:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@federalreserve"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 07:45:40"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247847452738236416",
            "tweetText" : "Was bei der Erstellung eines Webshops zu beachten ist, haben wir in einer Kurzanleitung für Sie zusammengefasst.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "kmu"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 11:20:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514844566597635",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 11:20:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 11:19:59"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240509624559828993",
            "tweetText" : "Collaborate like you’re sitting side-by-side on an online whiteboard. Free forever. No credit card required. https://t.co/O3CfanZE1z https://t.co/ycXjEEwcrg",
            "urls" : [ "https://t.co/O3CfanZE1z" ],
            "mediaUrls" : [ "https://t.co/ycXjEEwcrg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Graphics software"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "GitHub"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-05 01:31:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-05 01:31:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245709288162709505",
            "tweetText" : "If your #microservice architecture is not delivering on the promise of increased agility, scalability &amp; resiliency, download this guide: https://t.co/dvPoqcsMJw https://t.co/8eX6bPmYuG",
            "urls" : [ "https://t.co/dvPoqcsMJw" ],
            "mediaUrls" : [ "https://t.co/8eX6bPmYuG" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Solace",
            "screenName" : "@solacedotcom"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "enterprise architects"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-05 01:37:31"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240578761495425040",
            "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:22:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240158044413202434",
            "tweetText" : "【News】20,000+ Components in Seeed's OPL to Save Your Time and Money on Your PCB Assembly Order! Learn More 👉 https://t.co/qrSj1FOTtt https://t.co/gX8tJ8frrK",
            "urls" : [ "https://t.co/qrSj1FOTtt" ],
            "mediaUrls" : [ "https://t.co/gX8tJ8frrK" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Seeed Studio",
            "screenName" : "@seeedstudio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sparkfun"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@hackaday"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@arduino"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@adafruit"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:40:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1225134285335609345",
            "tweetText" : "type Query {\n  isGraphQL: FaunaDB!\n}\n\nTry FaunaDB: https://t.co/XqAAsPPPyv",
            "urls" : [ "https://t.co/XqAAsPPPyv" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fauna",
            "screenName" : "@fauna"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reactjs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JavaScriptDaily"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:27:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1248267112730234880",
            "tweetText" : "Arbeiten Mitarbeitende im #Homeoffice produktiver als im Büro? Mit den passenden Rahmenbedingungen und Tätigkeiten, ja!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom Business",
            "screenName" : "@Swisscom_B2B_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@derspiegel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@spiegelonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@faznet"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TEDTalks"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zeitonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SZ"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "work"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:27:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1208115770225377280",
            "tweetText" : "Security experts across all industries face the same challenge: how do I improve defenses against 🤖bot-generated traffic? 🤔\n\nIn our new #ebook, we take a look at ways attackers employ bots &amp; how your company can guard against bot attacks. Download --&gt; https://t.co/2Ldqmwri4G https://t.co/TfGV5HIHcI",
            "urls" : [ "https://t.co/2Ldqmwri4G" ],
            "mediaUrls" : [ "https://t.co/TfGV5HIHcI" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@owasp"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HackingDave"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@joshcorman"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WeldPond"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jack_daniel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@rapid7"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gattaca"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@metasploit"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jeremiahg"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@k8em0"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@securityweekly"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:33:59"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1093107212556345344",
            "tweetText" : "Private virtual Platform as a Service for developers.\nLots of features available from the box.\nTry for free!\n https://t.co/6cuN6zzRhI \n#d2cio #docker #4devs https://t.co/37UxQaNQxg",
            "urls" : [ "https://t.co/6cuN6zzRhI" ],
            "mediaUrls" : [ "https://t.co/37UxQaNQxg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "D2C.io",
            "screenName" : "@d2cio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Azure"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ThePSF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GCPcloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ubuntu"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Linux"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@debian"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@codinghorror"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "devops"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "linux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "golang"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "javascript"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "python"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#golang"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "13 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:40:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1248195078352506881",
            "tweetText" : "FREE Uptime Monitor &amp; Server Resource Usage Monitor\n\nGet alerts if your websites/servers go down, or if they start using too many resources\n\nAlerts via: Email SMS Telegram PushBullet Pushover Twitter Slack Discord Mattermost RocketChat MicrosoftTeams PagerDuty OpsGenie VictorOps",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "HetrixTools",
            "screenName" : "@HetrixTools"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zabbix"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:26:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247479734000406528",
            "tweetText" : "Welche Chancen haben Menschen ohne Zugang zu funktionierenden medizinischen und sanitären Einrichtungen im Kampf gegen #COVIDー19 ?\n\nGemeinsam müssen wir jenen helfen, die gefährdet sind.\n👉 https://t.co/SMynVT5eLg https://t.co/2L5gWypIVg",
            "urls" : [ "https://t.co/SMynVT5eLg" ],
            "mediaUrls" : [ "https://t.co/2L5gWypIVg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "IKRK",
            "screenName" : "@IKRK"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ICRC"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RotesKreuz_CH"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:22:37"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1103588856354533376",
            "tweetText" : "We are excited to introduce Mind Maps by Miro: a simple yet smart new tool!  Capture, organize and structure ideas quickly without switching between tools. https://t.co/bXOCWRQ6rl https://t.co/P4rOKGNK14",
            "urls" : [ "https://t.co/bXOCWRQ6rl" ],
            "mediaUrls" : [ "https://t.co/P4rOKGNK14" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:26:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247847388422815744",
            "tweetText" : "Im Moment kann ein Webshop sehr nützlich sein. Deshalb haben wir recherchiert und fassen in unserem Beitrag zusammen, wie Sie schnell und unbürokratisch zum eigenen Webshop gelangen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "kmu"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:25:01"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1230998681026883584",
            "tweetText" : "Learn how to get started with #DevSecOps with this checklist.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Azure"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlabstatus"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#logging"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:27:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245376969421848583",
            "tweetText" : "Es gibt viele Wege smart verbunden zu sein: Entdecke die Apple Watch mit Multi Device.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tim_cook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-04-10 06:27:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237646416677408768",
            "tweetText" : "Sie verstehen nichts von Hypotheken? Wir beraten Sie persönlich und kostenlos. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Retargeting campaign engager",
            "targetingValue" : "Retargeting campaign engager: 23109800"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:25:22"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1181307044550381569",
            "tweetText" : "Correlate logs with metrics and traces, easily investigate trends with the Log Patterns view, and now, re-index archived logs to provide historical context. Learn more here: https://t.co/MnoLgq4YuK",
            "urls" : [ "https://t.co/MnoLgq4YuK" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Datadog, Inc.",
            "screenName" : "@datadoghq"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@rapid7"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#DevOps"
          }, {
            "targetingType" : "Tailored audiences (lists)",
            "targetingValue" : "AWS Customers v2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:40:14"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Huawei Europe",
            "screenName" : "@Huawei_Europe"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:22:05"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237709976635916288",
            "tweetText" : "Finden Sie den Blick-Käfer? Jetzt mitmachen und Preise im Gesamtwert von über CHF 15'000.– gewinnen!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 14:39:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 14:38:59"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-24 07:29:34"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245267569755463681",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
            "urls" : [ "https://t.co/I4grLgGEqj" ],
            "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-24 07:29:55"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VIAVI Enterprise",
            "screenName" : "@ViaviEnterprise"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BlackHatEvents"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-24 07:26:54"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "The Cybersmile Foundation",
            "screenName" : "@CybersmileHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNICEF"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          } ],
          "impressionTime" : "2020-06-24 01:00:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275135887354089474",
            "tweetText" : "Chokes are EMC components that share aspects with inductors and transformers. Join our June 24 #webinar at 9 A.M. EDT/3 P.M. CEST to explore the construction and characteristics of different types of chokes: https://t.co/KLltIkHKrJ #KEMETishere for #engineers #engineering #tech https://t.co/ghSYh8hZ5w",
            "urls" : [ "https://t.co/KLltIkHKrJ" ],
            "mediaUrls" : [ "https://t.co/ghSYh8hZ5w" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "KEMET Electronics",
            "screenName" : "@KEMETCapacitors"
          },
          "impressionTime" : "2020-06-24 01:01:03"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Android",
            "deviceId" : "JgBihTt9AJY/IOC1OE+ZBFGAoCCQZC8exjS86CcDygs=",
            "deviceType" : "Samsung Galaxy Note III"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273598258934415360",
            "tweetText" : "Kopf versus Bauch – das sind die 4 häufigsten psychologischen Fallen beim Anlegen:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 01:54:06"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276138251322081281",
            "tweetText" : "Join leading academics and tech professionals from around the world to examine critical issues around AI for privacy and security at CyberSec&amp;AI Connected. JUNE 30 is your last chance for an early bird discount. Book now ➤ https://t.co/7ecFNlTenD https://t.co/jNEqup0RSM",
            "urls" : [ "https://t.co/7ecFNlTenD" ],
            "mediaUrls" : [ "https://t.co/jNEqup0RSM" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CyberSec&AI Connected",
            "screenName" : "@CyberSecAI"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Events",
            "targetingValue" : "Cyber Week 2020"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:44:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Milchproduzenten SMP / Producteurs de Lait PSL",
            "screenName" : "@SMP_PSL"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Government"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Entrepreneurship"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "switzerland"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:40:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Milchproduzenten SMP / Producteurs de Lait PSL",
            "screenName" : "@SMP_PSL"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Government"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Entrepreneurship"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "switzerland"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:37:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1270656956504641537",
            "tweetText" : "#Barrierefrei posten: In den Barrierefreiheit-Einstellungen \"Bildbeschreibungen verfassen\" aktivieren. Beim Posten eines Fotos unten rechts auf den +ALT-Button tippen und Text eingeben.\n#gemeinsambereit #bereit https://t.co/90SAooVT4y",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/90SAooVT4y" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@amnesty"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:37:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275813719935717377",
            "tweetText" : "Sie haben einen Covid-19 Kredit beansprucht? Dann brauchen Sie eine sorgfältig geplante Exit-Strategie und sollten sich bereits über die Zeit nach der Pandemie Gedanken machen. UBS steht Ihnen dabei mit Ratschlägen zur Seite: https://t.co/DHw2lSNF39 https://t.co/AdR6mKjHeE",
            "urls" : [ "https://t.co/DHw2lSNF39" ],
            "mediaUrls" : [ "https://t.co/AdR6mKjHeE" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Entrepreneurship"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Leadership"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:40:57"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SecurityWeek"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@threatpost"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@e_kaspersky"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-06-30 23:47:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1268255650313719808",
            "tweetText" : "How powerful is your application security? ⚡️Take the grader and get actionable steps to improve the security of your web apps. ✅",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "mongodb"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "redis"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "cloudflare"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "linux"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-06-30 23:47:55"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SecurityWeek"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@threatpost"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@e_kaspersky"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-01 10:39:46"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "BBB Studios",
            "screenName" : "@BBB_Studios"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Twitch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Treyarch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@aplusk"
          } ],
          "impressionTime" : "2020-07-01 21:00:00"
        } ]
      }
    }
  }
} ]